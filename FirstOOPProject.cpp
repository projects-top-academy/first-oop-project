#include <iostream>

class Elevator
{
public:
	Elevator(int lower, int upper) : lowerFloor(lower), upperFloor(upper), isWorking(false), currentFloor(1) {}

	void TurnOn() {
		isWorking = true;
	}

	void TurnOff() {
		isWorking = false;
	}

	std::string getStatus() {
		return isWorking ? "Working" : "Not working";
	}

	int getCurrentFloor() {
		return currentFloor;
	}

	void MoveToFloor(int targetfloor) {
		if (isWorking)
		{
			if (targetfloor >= lowerFloor && targetfloor <= upperFloor)
			{
				currentFloor = targetfloor;
				std::cout << "Elevator moved to " << targetfloor << " floor\n";
			}
			else
			{
				std::cout << "Invalid floor\n";
			}
		}
		else
		{
			std::cout << "Elevator is not working\n";
		}
	}

private:

	bool isWorking;
	int lowerFloor;
	int upperFloor;
	int currentFloor;
};

int main()
{
	Elevator elevator(1, 10);
	std::cout << "Current status: " << elevator.getStatus() << "\n";
	std::cout << "Current floor: " << elevator.getCurrentFloor() << "\n\n";

	elevator.TurnOn();
	std::cout << "Current status: " << elevator.getStatus() << "\n\n";

	elevator.MoveToFloor(5);
	std::cout << "Current floor: " << elevator.getCurrentFloor() << "\n\n";

	elevator.MoveToFloor(12);

	elevator.TurnOff();
	std::cout << "Current status: " << elevator.getStatus() << "\n";

	return 0;
}

/*

Задача

Создать класс "Лифт", представляющий собой предельно упрощенную модель лифта. Класс должен обеспечить:
установку диапазона движения лифта (нижний и верхний этаж);
включение/выключение лифта;
возвращение текущего состояния лифта(работает/не работает);
возвращение текущего положения лифта(этаж);
обработку вызова лифта(этаж).

Написать программу, тестирующую класс "Лифт"

*/